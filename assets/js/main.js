$(document).ready(function() {

    //----------------------------------------------------
    //--- ScrollTop ---
    $('a[href*="#"]:not([href="#"])').click(function() {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
            var target = $(this.hash);
            /*var menuHeight = 80;*/
            target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
            if (target.length) {
                $('html, body').animate({
                    scrollTop: target.offset().top /* - menuHeight*/
                }, 1000);
                return false;
            }
        }
    });
    //--- //ScrollTop ---
    //----------------------------------------------------


    $('.sp-menu .linkrow-s .toggle-sign').click(function() {
        $(this).find('img').toggleClass('hide');
        var toggleId = $(this).find('img').attr('toggle-for');
        $('.sp-menu .' + toggleId).toggleClass('hide');
    });

    $('.navbar-toggler').click(function() {
        // $('#header > .navbar > .container').toggleClass('scroll');
    });


    $(".slider").slick({
        dots: true,
        arrows: false,
        speed: 1500,
        fade: true,
        autoplaySpeed: 5500,
        autoplay: true
    });



    //----------------------------------------------------
    //---SLICK SLIDE---

    $('.restr-slick-slide').slick({
        slidesToShow: 1,
        dots: true,
        arrows: false,
        infinite: true,
        autoplay: true,
        autoplaySpeed: 2000,
    });

    $('.sd-dn-slick-slider').slick({
        slidesToShow: 1,
        dots: true,
        arrows: true,
        infinite: true,
        autoplay: true,
        autoplaySpeed: 2000,
    });



     //--- //SLICK SLIDE ---
    //----------------------------------------------------



    //-------------------- Navigation SP -----------------

    $('#navbarSupportedContent').on('shown.bs.collapse', function () {
        $(".navbar").addClass("show-nav-sp");
    }).on('hide.bs.collapse', function () {
        $(".navbar").removeClass("show-nav-sp");
    });

    $( window ).resize(function() {
        var w = $( window ).width();

        if(w > 1006) {
            $(".navbar").removeClass("show-nav-sp");
        }
    });

    //-------------------- //Navigation SP -----------------


});


//---------------- Setup google map api ----------------------

function initMap() {
    var styles = [
        {
            featureType: "administrative",
            elementType: "all",
            stylers: [
                { saturation: -100 }
            ]
        },{
            featureType: "landscape",
            elementType: "all",
            stylers: [
                { saturation: -100 }
            ]
        },{
            featureType: "poi",
            elementType: "all",
            stylers: [
                { saturation: -100 }
            ]
        },{
            featureType: "road",
            elementType: "all",
            stylers: [
                { saturation: -100 }
            ]
        },{
            featureType: "transit",
            elementType: "all",
            stylers: [
                { saturation: -100 }
            ]
        },{
            featureType: "water",
            elementType: "all",
            stylers: [
                { saturation: -100 }
            ]
        }
    ];


    // Create a map object, and include the MapTypeId to add
    // to the map type control.
    var map = new google.maps.Map(document.getElementById('map'), {
        zoom: 18,
        center: {lat: 34.6868839, lng: 135.1969075},
        disableDefaultUI: true
    });

    map.setOptions({
        styles: styles
    });

    var image = './img/top/map-maker.png';
    var beachMarker = new google.maps.Marker({
        position: {lat: 34.6868839, lng: 135.1969075},
        //position: {lat: -33.890, lng: 151.274},
        map: map,
        icon: image
    });
}

//---------------- //Setup google map api ----------------------
